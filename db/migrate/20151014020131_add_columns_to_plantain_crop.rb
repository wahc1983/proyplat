class AddColumnsToPlantainCrop < ActiveRecord::Migration
  def change
  	add_column :plantain_crops, :town, :string, null: false
  	add_column :plantain_crops, :harton, :boolean, default: false
  	add_column :plantain_crops, :guineo, :boolean, default: false
  	add_column :plantain_crops, :macho, :boolean, default: false
  	add_column :plantain_crops, :rojo, :boolean, default: false
  	add_column :plantain_crops, :valery, :boolean, default: false
  	add_column :plantain_crops, :kind_land, :string, null: false
  end
end
