class ChangeColumsFromUser < ActiveRecord::Migration

	change_table :users do |t|

		t.rename :nombre, :name
		t.rename :doc_ident, :identity_number

	end

	def change

		add_column :users, :is_admin, :boolean, default: false

	end

end
