class AddColumnsToBlog < ActiveRecord::Migration
  def change
  	add_column :blogs, :title, :string, null: false
  	add_column :blogs, :content, :string, null: false
  end
end
