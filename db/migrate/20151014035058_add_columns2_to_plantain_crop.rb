class AddColumns2ToPlantainCrop < ActiveRecord::Migration
  def change
  	add_column :plantain_crops, :depth_range, :string, null: false
  	add_column :plantain_crops, :weather_range, :string, null: false
  	add_column :plantain_crops, :has_irrigation, :boolean, default: false 
  	add_column :plantain_crops, :planting_distance, :string, null: false
  	change_column_null :plantain_crops, :name, false
  	change_column_null :plantain_crops, :location, false
  	change_column_null :plantain_crops, :size, false
  end
end
