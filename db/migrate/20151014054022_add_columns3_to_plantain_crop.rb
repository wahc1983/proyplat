class AddColumns3ToPlantainCrop < ActiveRecord::Migration
  def change
  	add_column :plantain_crops, :has_sterilization, :boolean, default: false
  	add_column :plantain_crops, :has_weed_control, :boolean, default: false
  	add_column :plantain_crops, :leaf_removal, :string, null: false
  	add_column :plantain_crops, :fungicide, :string, null: false
  	execute 'ALTER TABLE plantain_crops ALTER COLUMN size TYPE decimal USING (size::decimal)'
  end
end
