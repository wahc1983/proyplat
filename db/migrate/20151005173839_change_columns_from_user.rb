class ChangeColumnsFromUser < ActiveRecord::Migration
  def change
  	change_column_null :users, :name, false
  	change_column_null :users, :identity_number, false
  end
end
