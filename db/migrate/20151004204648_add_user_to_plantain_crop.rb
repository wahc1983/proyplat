class AddUserToPlantainCrop < ActiveRecord::Migration
  def change
    add_reference :plantain_crops, :user, index: true, foreign_key: true
  end
end
