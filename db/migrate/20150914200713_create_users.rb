class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :nombre
      t.integer :doc_ident
      t.string :email

      t.timestamps null: false
    end
  end
end
