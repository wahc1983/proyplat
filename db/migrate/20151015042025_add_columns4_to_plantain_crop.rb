class AddColumns4ToPlantainCrop < ActiveRecord::Migration
  def change
  	add_column :plantain_crops, :underpinned, :string, null: false
  	add_column :plantain_crops, :sheathed, :string, null: false
  	add_column :plantain_crops, :tanks_verification, :boolean, default: false
  	add_column :plantain_crops, :clusters_processing, :boolean, default: true
  	add_column :plantain_crops, :clusters_selecting, :boolean, default: false
  	add_column :plantain_crops, :clusters_packing, :boolean, default: false
  	add_column :plantain_crops, :clusters_selecting_with_instruments, :boolean, default: false 	
  	add_column :plantain_crops, :finger_size, :string, null: false
  end
end
