# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151022133652) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "blogs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title",      null: false
    t.string   "content",    null: false
    t.integer  "user_id"
  end

  add_index "blogs", ["user_id"], name: "index_blogs_on_user_id", using: :btree

  create_table "homes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "plantain_crops", force: :cascade do |t|
    t.string   "name",                                                null: false
    t.string   "location",                                            null: false
    t.decimal  "size",                                                null: false
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "user_id"
    t.string   "town",                                                null: false
    t.boolean  "harton",                              default: false
    t.boolean  "guineo",                              default: false
    t.boolean  "macho",                               default: false
    t.boolean  "rojo",                                default: false
    t.boolean  "valery",                              default: false
    t.string   "kind_land",                                           null: false
    t.string   "depth_range",                                         null: false
    t.string   "weather_range",                                       null: false
    t.boolean  "has_irrigation",                      default: false
    t.string   "planting_distance",                                   null: false
    t.boolean  "has_sterilization",                   default: false
    t.boolean  "has_weed_control",                    default: false
    t.string   "leaf_removal",                                        null: false
    t.string   "fungicide",                                           null: false
    t.string   "underpinned",                                         null: false
    t.string   "sheathed",                                            null: false
    t.boolean  "tanks_verification",                  default: false
    t.boolean  "clusters_processing",                 default: true
    t.boolean  "clusters_selecting",                  default: false
    t.boolean  "clusters_packing",                    default: false
    t.boolean  "clusters_selecting_with_instruments", default: false
    t.string   "finger_size",                                         null: false
    t.integer  "score",                               default: 0
  end

  add_index "plantain_crops", ["user_id"], name: "index_plantain_crops_on_user_id", using: :btree

  create_table "potential_customers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "phone"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "towns", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.integer  "identity_number",                        null: false
    t.string   "email",                                  null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "is_admin",               default: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  add_foreign_key "blogs", "users"
  add_foreign_key "plantain_crops", "users"
end
