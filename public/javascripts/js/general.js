$(document).ready(function() {
	$('.plugin_container select').change(function() {
		var select_val = $(this).val();
		$('.supersocialshare.bubble').each(function() {
			var extra_class = ($(this).hasClass('left')? 'left ' : 'right ') + select_val;

			$(this).attr('class', 'supersocialshare bubble '+extra_class);
		});
	});
});