/* 
latest update : Mar/14/2014

    event: the name of the event (or space-delimited list of names) to bind, e.g. "keyup".
    object_id: name of the object to which the event is assigned.
    query_name: variable name that contain the query to search.
    delay: the number of milliseconds to wait after the event before maybe kicking off the request, e.g. 100
    url: the url to fetch.
    action: the HTTP method to use for the request, e.g. "get" or "post"; defaults to "get".
    params: hash with the parameters that were sent in the request.
    callback: the function to invoke with the return result from the server.
    spinner_id: spinner name associated with the loading of data.
    update: boolean variable that express if the user allow the update action.

*/
(function($){
  $.fn.liveSearchDelayed = function(event, object_id, query_name, delay, url, action, params, callback, spinner_id, update){
      
    var object = jQuery('#'+object_id);

    if (query_name == ''){
        var query = 'query';
    }else{
        var query = query_name;
    }

    if(spinner_id == ''){
        var spinner = jQuery('#spinner');
    }else{
        var spinner = jQuery('#'+spinner_id);
    }

    var xhr, timer;
    return this.bind(event,function(){
      clearTimeout(timer);
      data = {};
      if (xhr) xhr.abort();
      timer = setTimeout(function(){spinner.show();
                                    if (object.val().length == 0 && update == true) {
                                            spinner.hide();
                                            callback.call(this,'');
                                            return false;
                                     } else {

                                            if (params != ''){
                                                params.call(this);
                                            }
                                            data[query]=object.val();
                                            xhr = $.ajax({
                                                          type:action||'get',
                                                          url:url,
                                                          data:data,
                                                          success:function(data){
                                                                                  xhr = null;
                                                                                  spinner.hide();
                                                                                  callback.call(this,data);
                                                                                 }
                                                          });

                                    }

                                    },delay);
    });
  };
})(jQuery);