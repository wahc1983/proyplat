json.array!(@users) do |user|
  json.extract! user, :id, :nombre, :doc_ident, :email
  json.url user_url(user, format: :json)
end
