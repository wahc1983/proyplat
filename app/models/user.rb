class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :plantain_crops
  has_many :blogs

  validates :name, :identity_number, presence: true

  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  def confirm!
    welcome_email
    super
  end

  def welcome_email
  	puts '*'*20
  	puts 'enviar email'
  	puts '*'*20
    EmailSender.welcome_new_user(self).deliver
  end  
end
