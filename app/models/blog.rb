class Blog < ActiveRecord::Base
	belongs_to :user
	validates :title, :content, presence: true

	public
  def self.search(query)
    if !query.to_s.strip.empty?
      tokens = query.split.collect {|c| "%#{c.downcase}%"}
      conditions = [(["(LOWER(content) LIKE ? OR LOWER(title) LIKE ?)"] * tokens.size).join(" AND "), *tokens.collect { |token| [token] * 2 }.flatten]
      Blog.where(conditions)
    else
      []
    end
  end

end
