class PlantainCrop < ActiveRecord::Base
	
	belongs_to :user

	validates :name, :location, :size, :town, :kind_land, :depth_range, :weather_range, 
            :planting_distance, :leaf_removal, :fungicide, :underpinned, :sheathed, 
            :finger_size, presence: true

  after_create :score_verification

  WEIGHTED_NULL = 0
  WEIGHTED_LOW 	= 1
  WEIGHTED_HALF =	3
  WEIGHTED_HIGH	= 5
  WEIGHTED_TOTAL = 50
  RANGE_LOW = [0, 17]
  RANGE_HALF = [18, 33]
  RANGE_HIGH = [34, 50]

  def self.plantains_in_crop(plantain_crop)

    kind_plantains_y = [] 
    kind_plantains_n = []
 
    if plantain_crop.guineo
      kind_plantains_y << 'Guineo'
    else
      kind_plantains_n << 'Guineo'
    end
    if plantain_crop.harton
      kind_plantains_y << 'Hartón'
    else
      kind_plantains_n << 'Hartón'
    end
    if plantain_crop.macho
      kind_plantains_y << 'Macho'
    else
      kind_plantains_n << 'Macho'
    end
    if plantain_crop.rojo
      kind_plantains_y << 'Rojo'
    else
      kind_plantains_n << 'Rojo'
    end
    if plantain_crop.valery
      kind_plantains_y << 'Valery'
    else
      kind_plantains_n << 'Valery'
    end

    return kind_plantains_y, kind_plantains_n

  end

  def ranking_verifation
    
  end

  private
  def score_verification
    
    in_crop = self
    new_score = in_crop.score
    if in_crop.guineo || in_crop.harton || in_crop.macho || in_crop.rojo || in_crop.valery
      new_score += WEIGHTED_HALF 
    end

    unless in_crop.kind_land == 'Ninguno'
      new_score += WEIGHTED_HIGH
    end

    if in_crop.depth_range == '1.2-1.5'
      new_score += WEIGHTED_LOW
    end

    if in_crop.weather_range == '22-27'
      new_score += WEIGHTED_LOW
    end

    if in_crop.has_irrigation
      new_score += WEIGHTED_HIGH
    end

    if in_crop.has_sterilization
      new_score += WEIGHTED_HIGH
    end

    if in_crop.has_weed_control
      new_score += WEIGHTED_HIGH
    end

    unless in_crop.leaf_removal == 'Ninguno'
      new_score += WEIGHTED_HALF
    end

    unless in_crop.fungicide == 'Ninguno'
      new_score += WEIGHTED_HIGH
    end

    unless in_crop.tanks_verification
      new_score += WEIGHTED_HALF
    end

    if in_crop.clusters_selecting
      new_score += WEIGHTED_HALF
    end

    if in_crop.clusters_selecting_with_instruments
      new_score += WEIGHTED_HALF
    end

    if in_crop.clusters_packing
      new_score += WEIGHTED_HALF
    end

    if in_crop.finger_size == 'mas de 10 pulgadas'
      new_score += WEIGHTED_HIGH
    end

    p '*'*200
    puts 'new_score'
    p new_score
    in_crop.score = new_score
    in_crop.save!
    puts in_crop.score

  end

end
