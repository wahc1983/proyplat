class PotentialCustomer < ActiveRecord::Base

	validates :name, :email, presence: true

end
