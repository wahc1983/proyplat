module ApplicationHelper

  #------------------------------------------------------
  # our_observe_field
  # field_id: object id to which the event is assigned.
  # options:{ query_name, spinner_id, delay, url, before, update_id, update_effect, success, with}
  #------------------------------------------------------
  def our_observe_field(field_id, options = {})

     url_options = options[:url]
     url_options = url_options.merge(:escape => false) if url_options.is_a?(Hash)

     javascript  = "jQuery(document).ready(function($){"
     javascript << " var script = '/javascripts/jquery/jquery.liveSearchDelayed_v2.js';"
     javascript << "jQuery.getScript(script, function() {"
     javascript << "var obj_id= '#{field_id}'; "
     javascript << "var object = jQuery('##{field_id}'); "
     javascript << "var query_name = '#{options[:query_name]}'; "
     javascript << "var spinner_id = '#{options[:spinner_id]}'; "
     javascript << "var delay = '#{options[:delay]}'; "
     javascript << "var url_to_search_list = '#{escape_javascript(url_for(url_options))}'; "

     if !options[:with].nil?
      javascript << "var params = function(html){data = #{options[:with]}}; "
     else
      javascript << "var params = ''; "
     end

     if !options[:update_id].nil?
      if !options[:update_effect].nil?
       javascript << "var callback =  function(html){#{options[:before]}; jQuery('##{options[:update_id]}').html(html).#{options[:update_effect]}(400, function(){ #{options[:success]}});}; "
       javascript << "var update = true; "
      else
       javascript << "var callback =  function(html){#{options[:before]}; jQuery('##{options[:update_id]}').html(html).show(400, function(){ #{options[:success]}});}; "
       javascript << "var update = true; "
      end
     else
       javascript << "var callback = function(html){#{options[:before]}}; "
       javascript << "var update = false; "
     end

     javascript << "object.liveSearchDelayed('keyup',obj_id,query_name,delay,url_to_search_list,'post',params,callback,spinner_id,update);});});"

     return javascript_tag(javascript)
  end
	
  def our_notice_alert(notice)
    unless notice.nil?
      r = []
      r << '<h4 id="notice" class="alert alert-success .alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>'+ notice +'</h4>'
      return r.join('').html_safe
    end  
  end

end
