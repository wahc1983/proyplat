module PlantainCropsHelper

	public

	def self.advantages_of_cultivated_plantains(in_crop, kind_plantains_y, kind_plantains_n)
		r = []
		unless kind_plantains_y.nil?
			r << "<p><i class='fa fa-asterisk'></i> En tu terreno estas cultivando #{kind_plantains_y.size.to_s} tipo"
			r << "s" if kind_plantains_y.size >1
			r << " de platano perfectos para la región y los cuales son: </p>"
			plantain_description(kind_plantains_y, r)
		end

		if !kind_plantains_y.nil? && !kind_plantains_n.nil?
			r << '<p><i class="fa fa-asterisk"></i> Tambien te recomendamos'
			r << 'este otro tipo' if kind_plantains_n.size == 1
			r << 'estos otros tipos' if kind_plantains_n.size > 1
			r << ' de platano que puede ser una opción para otras cosechas: </p>'
			plantain_description(kind_plantains_n, r)
		end

		if kind_plantains_y.nil? && !kind_plantains_n.nil?
			r << '<p><i class="fa fa-asterisk"></i> Te recomendamos cultivar'
			r << 'este otro tipo' if kind_plantains_n.size == 1
			r << 'estos otros tipos' if kind_plantains_n.size > 1
			r << ' de platano que puede ser una opción para otras cosechas: </p>'
			plantain_description(kind_plantains_n, r)
		end		

		return r.join('').html_safe
	end

	def self.plantain_description(kind_plantains, r)

			kind_plantains.each {|kind_plantain|
			p kind_plantain
			r << case kind_plantain
			when 'Guineo'
				'<p> -Guineo: oriundo del Asia sudoriental, actualmente Brasil y Kenia 
										 lo exportan durante todo el año. El fruto mide de 8 a 10cm, 
										 de piel muy fina y con cierto sabor a manzana. </p>'
			when 'Hartón'
				'<p> -Hartón: de características excepcionales para la exportación, 
											por cumplir con todos los requisitos de calidad exigidos; presenta además ventajas comparativas 
											por ser de porte bajo, característica que lo hace ideal para zonas de vientos fuertes. </p>'
			when 'Macho'
				'<p> -Macho: También llamado banana grande o de guisar. No es muy conocido en Europa, 
											sin embargo en muchos países tropicales es un alimento básico. Se emplea verde en diversas elaboraciones culinarias, 
											como por ejemplo cocido, frito, a la plancha o en papilla, pero no se come crudo. 
											Suele ser de mayor tamaño y más anguloso que los plátanos estándar. De coloración verde, amarilla o violácea. </p>'
			when 'Rojo'
				'<p> -Rojo: Especialidad rara de ver de la parte sudoriental de Asia; se consume preferentemente 
											caliente, porque el calor acentúa las cualidades gustativas de la pulpa roja. Presenta una piel roja, rosa o con 
											mezcla de verde. </p>'
			when 'Valery'
				'<p> -Valery: Frutos grandes sabor dulce y consistente. El nombre está registrado como marca. </p>'										 									 										 										 
			else
				''
			end
		}
		return r
	end

	def self.advantages_of_kind_land(in_crop)
		r = []
		if in_crop.kind_land == 'Ninguno'
			r << '<p><i class="fa fa-asterisk"></i> Los suelos aptos para el desarrollo del cultivo del plátano son aquellos 
						ue presentan una textura franco arenosa, franco arcillosa, franco arcillo limosa 
						y franco limosa, debiendo ser, además, fértiles y permeables. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> El suelo de tu terreno es apto para el cultivo del platano. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_depth_range(in_crop)
		r = []
		if in_crop.depth_range == '1.2-1.5'
			r << '<p><i class="fa fa-asterisk"></i> Tu terreno tiene la profundidad adecuada. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> Tu terreno no tiene la profundidad adecuada. </p>'
		end
		return r.join('').html_safe
	end

	def self.analysis_weather_range(in_crop)
		r = []
		if in_crop.weather_range == '22-27'
			r << '<p><i class="fa fa-asterisk"></i> El rango de temperatura para tu cultivo es el adecuado. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> El rango de temperatura para tu cultivo no es el adecuado. 
						El plátano exige un clima cálido y una constante humedad en el aire. 
						Necesita una temperatura media de 26-27 ºC, con lluvias prolongadas y regularmente 
						distribuidas</p>'
		end
		return r.join('').html_safe		
	end

	def self.analysis_has_irrigation(in_crop)
		r = []
		if in_crop.has_irrigation
			r << '<p><i class="fa fa-asterisk"></i> Tu cultivo si posee irrigación adecuada y esto ayuda a la calidad de tu producto. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> Los plátanos requieren mucha agua, pero corren el 
						riesgo de podrirse si el agua no se drena adecuadamente, le recomendamos implementar 
						un sistema de drenaje adecuado.</p>'
		end
		return r.join('').html_safe		
	end

	def self.analysis_has_sterilization(in_crop)
		r = []
		if in_crop.has_sterilization
			r << '<p><i class="fa fa-asterisk"></i> Si se esteriliza la semilla ó rizoma. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> La Semilla (rizoma), debe ser pelada, eliminando la tierra adherida a ella, 
						raíces y todo tejido dañado por picudo u otros insectos. Una vez limpia, debe ser desinfectada para que quede 
						libre de patógenos.
						Se le debe hacer una inmersión de la semilla por 5-10 minutos en una mezcla de insecticida- fungicida.</p>'
		end
		return r.join('').html_safe		
	end

	def self.analysis_has_weed_control(in_crop)
		r = []
		if in_crop.has_weed_control
			r << '<p><i class="fa fa-asterisk"></i> En tu cultivo se realiza un control de plagas adecuado y esto ayuda a la calidad de tu producto. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> Las malezas compiten con el cultivo por agua, luz y nutrientes, 
						además muchas son hospederas de enfermedades e insectos plagas. El manejo de las 
						malezas se debe realizar mediante la integración de métodos culturales, mecánicos 
						y químicos. Su efectividad dependerá de la oportunidad y eficiencia con que se 
						realicen.</p>'
		end
		return r.join('').html_safe		
	end

	def self.analysis_leaf_removal(in_crop)
		r = []
		if in_crop.leaf_removal == 'Ninguno'
			r << '<p><i class="fa fa-asterisk"></i> El deshoje tiene como objetivo la eliminación de hojas dobladas, 
						maduras e infectadas por sigatoka. Se tienen entonces dos tipos de deshoje: el de sanidad, 
						que remueve hojas no funcionales bien sea por culminación de su ciclo, daños mecánicos o por enfermedad (S. negra) 
						y el de protección del racimo, que consiste en eliminar las hojas o partes de ella que pegan al racimo produciéndole 
						cicatrización. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> Utilizas el deshoje en tu cultivo, es una buena práctica para la calidad de tu producto. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_fungicide(in_crop)
		r = []
		if in_crop.fungicide == 'Ninguno'
			r << "<p><i class='fa fa-asterisk'></i> Debes utilizar un fungicida para mejorar la calidad de tu cultivo. 
						La siguiente tabla te muesta algunos de ellos: </p>
						<img src='#{SiteAdmin::K_SITE_ADDRESS}/images/fungicidas.jpg' class='img-responsive'>"

		else
			r << '<p><i class="fa fa-asterisk"></i> Utilizas un fungicida en tu cultivo, es una buena práctica para la calidad de tu producto. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_tanks_verification(in_crop)
		r = []
		unless in_crop.tanks_verification
			r << '<p><i class="fa fa-asterisk"></i> Antes de iniciar el corte verifique que los tanques de la empacadora 
					estén listos con agua-alumbre y agua - merteck. </p>'

		else
			r << '<p><i class="fa fa-asterisk"></i> La verificación de los tanques de la empacadora 
					estén listos con agua-alumbre y agua - merteck es una buena práctica para la calidad de tu producto. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_clusters_selecting(in_crop)
		r = []
		unless in_crop.clusters_selecting
			r << '<p><i class="fa fa-asterisk"></i> No procese racimos de plantas con menos de siete hojas funcionales. </p>'

		else
			r << '<p><i class="fa fa-asterisk"></i> El no procesar racimos de plantas con menos de siete hojas funcionales es 
						una buena práctica para la calidad de tu producto. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_clusters_selecting_with_instruments(in_crop)
		r = []
		if in_crop.clusters_selecting_with_instruments
			r << '<p><i class="fa fa-asterisk"></i> Seleccionar la fruta con calibrador y cinta métrica es 
						una buena práctica para la calidad de tu producto. </p>'

		else
			r << '<p><i class="fa fa-asterisk"></i> Seleccione la fruta con calibrador y cinta métrica para mejorar la calidad de su producto. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_clusters_packing(in_crop)
		r = []
		unless in_crop.clusters_packing
			r << '<p><i class="fa fa-asterisk"></i> No empaque coronas desgarradas, Desmane por debajo de la línea oscura que une 
						los dedos con el vástago, procurando que los plátanos caigan sueltos al tanque. Con este corte se obtienen coronas rectas 
						y limpias. </p>'

		else
			r << '<p><i class="fa fa-asterisk"></i> El no empacar coronas desgarradas es una buena practica para la calidad de su producto. </p'
		end
		return r.join('').html_safe
	end

	def self.analysis_finger_size(in_crop)
		r = []
		if in_crop.finger_size == 'mas de 10 pulgadas'
			r << '<p><i class="fa fa-asterisk"></i> Cosechar dedos de mas de 10 pulgadas es una buena practica para la calidad de su producto. </p>'
		else
			r << '<p><i class="fa fa-asterisk"></i> Cada dedo debe medir mínimo 10 pulgadas de pulpa a punta 
						y debe tener de 22 a 28 líneas de vitola. </p>'
		end
		return r.join('').html_safe
	end

	def self.analysis_range(in_crop)

		range_low = PlantainCrop::RANGE_LOW
		range_half = PlantainCrop::RANGE_HALF
		range_high = PlantainCrop::RANGE_HIGH
		score = in_crop.score		
		r = []

		r << case score
		when range_low.first..range_low.last
			'<p class="description" >Con este puntaje tu cultivo esta en un rango <strong>Bajo</strong>. Mira nuestras recomendaciones 
			y mejora tus practicas agropecuarias.</p>
			 <p class="description" >Luego edita los datos de tu cultivo con los cambios que has hecho y mejora tu 
			 puntuación.</p>'
		when range_half.first..range_half.last
			'<p class="description" >Con este puntaje tu cultivo esta en un rango <strong>Medio</strong>. Mira nuestras recomendaciones 
			y mejora tus practicas agropecuarias.</p>
			 <p class="description" >Luego edita los datos de tu cultivo con los cambios que has hecho y mejora tu 
			 puntuación.</p>'			
		when range_high.first..range_high.last
			'<p class="description" >Con este puntaje tu cultivo esta en un rango <strong>Alto</strong>. Te felicitamos por tus
			buenas practicas agropecuarias.</p>
			 <p class="description" >Puedes ayudar a otros a mejorar comentando en nuestro blog y compartiendo tus experiencias y conocimiento.</p>'			
		else
			'<p>Tu cultivo no tiene puntuación aún.</p>'
		end

		return r.join('').html_safe
	end

end
