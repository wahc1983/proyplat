class MyDevise::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters
  layout "devise"

	def after_inactive_sign_up_path_for(resource)
		return user_session_path
	end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:name, :identity_number, :email)
  end
end