class LiveController < ApplicationController
	
	layout false

  def search

    @search = params[:q]
    @articles = Blog.search(@search)
    headers["Content-Type"] = "text/html; charset=utf-8"

  end
end
