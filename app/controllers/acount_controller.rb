class AcountController < ApplicationController
	
	before_action :authenticate_user!

  def index
  	@blogs = current_user.blogs.paginate(:page => params[:page], :per_page => 10)
  	@plantain_crops = current_user.plantain_crops.reorder("created_at DESC").paginate(:page => params[:page], :per_page => 3)
  end
end
