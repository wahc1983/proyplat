class PlantainCropsController < ApplicationController
  before_action :set_plantain_crop, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :is_user_admin, only: [:index]

  # GET /plantain_crops
  # GET /plantain_crops.json
  def index
    @plantain_crops = PlantainCrop.all
  end

  # GET /plantain_crops/1
  # GET /plantain_crops/1.json
  def show
  end

  # GET /plantain_crops/new
  def new
    @plantain_crop = PlantainCrop.new
  end

  # GET /plantain_crops/1/edit
  def edit
  end

  # POST /plantain_crops
  # POST /plantain_crops.json
  def create
    
    #@plantain_crop = PlantainCrop.new(plantain_crop_params)
    @plantain_crop = current_user.plantain_crops.create(plantain_crop_params)

    respond_to do |format|
      if @plantain_crop.save
        
        kind_plantains_y , kind_plantains_n = PlantainCrop::plantains_in_crop(@plantain_crop)
        EmailSender.analysis_crop(current_user, @plantain_crop, kind_plantains_y , kind_plantains_n).deliver

        format.html { redirect_to @plantain_crop, notice: 'El cultivo fue creado con éxito.' }
        format.json { render :show, status: :created, location: @plantain_crop }
      else
        format.html { render :new }
        format.json { render json: @plantain_crop.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plantain_crops/1
  # PATCH/PUT /plantain_crops/1.json
  def update
    respond_to do |format|
      if @plantain_crop.update(plantain_crop_params)
        format.html { redirect_to @plantain_crop, notice: 'El cultivo se ha actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @plantain_crop }
      else
        format.html { render :edit }
        format.json { render json: @plantain_crop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plantain_crops/1
  # DELETE /plantain_crops/1.json
  def destroy
    @plantain_crop.destroy
    respond_to do |format|
      format.html { redirect_to plantain_crops_url, notice: 'El cultivo fue destruido con éxito.' }
      format.json { head :no_content }
    end
  end

  def crop_analysis
    crop_id = params[:id]
    @plantain_crop = PlantainCrop.find(crop_id)

    kind_plantains_y , kind_plantains_n = PlantainCrop::plantains_in_crop(@plantain_crop)

    render locals: {kind_plantains_y: kind_plantains_y, kind_plantains_n: kind_plantains_n}    
  end

  private

    def is_user_admin
      unless current_user.is_admin == true
        redirect_to root_path
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_plantain_crop
      @plantain_crop = PlantainCrop.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plantain_crop_params
      params.require(:plantain_crop).permit(:name, :location, :size, 
                                            :town, :guineo, :harton, 
                                            :macho, :rojo, :valery, 
                                            :kind_land, :depth_range, :weather_range, 
                                            :has_irrigation, :planting_distance, :has_sterilization, 
                                            :has_weed_control, :leaf_removal, :fungicide,
                                            :underpinned, :sheathed, :finger_size,
                                            :tanks_verification, :clusters_processing, :clusters_selecting, 
                                            :clusters_packing, :clusters_selecting_with_instruments, :score)
    end
end
