class ContactUsController < ApplicationController

	def index
		
		@potential_customer = PotentialCustomer.new
		render layout: 'home2', locals: {notice: params[:notice]}

	end

  def create
    
    @potential_customer = PotentialCustomer.new(contact_us_params)

    respond_to do |format|
      if @potential_customer.save

        EmailSender.new_potential_customer(@potential_customer).deliver

        format.html { redirect_to action: 'index', notice: 'Gracias por tu mensaje' }
        format.json { render :show, status: :created, location: @potential_customer }
      else
        format.html { render :index }
        format.json { render json: @potential_customer.errors, status: :unprocessable_entity }
      end
    end  	
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_us_params
      params.require(:potential_customer).permit(:name, :email, :phone, :message)
    end

end
