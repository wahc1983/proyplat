class EmailSender < ApplicationMailer
	default from: "betterplantain@gmail.com"

	def welcome_new_user(user)
		@user = user
   		mail(to: @user.email, subject: 'Bienvenido '+@user.name+' a Better Plantain')	
	end

	def analysis_crop(user, crop, kind_plantains_y , kind_plantains_n)
		@user = user
		@plantain_crop = crop
		@kind_plantains_y = kind_plantains_y
		@kind_plantains_n = kind_plantains_n
		mail(to: @user.email, subject: 'Analisis del cultivo '+@plantain_crop.name+' creado en Better Plantain')
	end

	def new_potential_customer(potential_customer)
		@potential_customer = potential_customer
   		mail(to: 'betterplantain@gmail.com', subject: 'un posible cliente '+@potential_customer.name+' entró en Better Plantain')	
	end

end
