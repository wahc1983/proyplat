# Preview all emails at http://localhost:3000/rails/mailers/email_sender
class EmailSenderPreview < ActionMailer::Preview

  def welcome_new_user_preview
    EmailSender.welcome_new_user(User.first)
  end

  def new_potential_customer_preview
    EmailSender.new_potential_customer(PotentialCustomer.first)
  end

  def analysis_crop_preview
  	@plantain_crop = PlantainCrop.first
  	kind_plantains_y , kind_plantains_n = PlantainCrop::plantains_in_crop(@plantain_crop)
    EmailSender.analysis_crop(User.first, @plantain_crop, kind_plantains_y , kind_plantains_n)
  end

end
