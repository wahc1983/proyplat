require 'test_helper'

class PlantainCropsControllerTest < ActionController::TestCase
  setup do
    @plantain_crop = plantain_crops(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:plantain_crops)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create plantain_crop" do
    assert_difference('PlantainCrop.count') do
      post :create, plantain_crop: { location: @plantain_crop.location, nanme: @plantain_crop.nanme, size: @plantain_crop.size }
    end

    assert_redirected_to plantain_crop_path(assigns(:plantain_crop))
  end

  test "should show plantain_crop" do
    get :show, id: @plantain_crop
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @plantain_crop
    assert_response :success
  end

  test "should update plantain_crop" do
    patch :update, id: @plantain_crop, plantain_crop: { location: @plantain_crop.location, nanme: @plantain_crop.nanme, size: @plantain_crop.size }
    assert_redirected_to plantain_crop_path(assigns(:plantain_crop))
  end

  test "should destroy plantain_crop" do
    assert_difference('PlantainCrop.count', -1) do
      delete :destroy, id: @plantain_crop
    end

    assert_redirected_to plantain_crops_path
  end
end
